import { Container } from './{{properCase name}}Container.js';
import { {{properCase name}} as Component } from './{{properCase name}}.jsx';

const {{properCase name}} = Container(Component);

export { {{properCase name}} };
