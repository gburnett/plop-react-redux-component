import React from 'react';
import { render, cleanup } from '@testing-library/react';
import '@testing-libary/jest-dom/extend-expect';
import { createProvider } from '../../store/createProvider';
import { {{properCase name}}Container } from './{{properCase name}}Container';

const setup = state => {
  const Provider = createProvider(state);
  return render(<{{properCase name}}Container />, { wrapper: Provider });
};

describe('components/{{properCase name}}/{{properCase name}}Container', () => {
  let queryByText;
  beforeEach(() => {
    const state = {};
    ({queryByText} = setup(state));
  });

  afterEach(cleanup);

  it('fails this example', () => {
    expect(queryByText('something you will not find')).toBeInTheDocument();
  });
});
