import React from 'react';
import { render, cleanup } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { {{properCase name}} } from './{{properCase name}}';

const setup = (props = {}) => render(<{{properCase name}} {...props} />);

describe('components/{{properCase name}}/{{properCase name}}', () => {
  let queryByText;

  beforeEach(() => {
    ({ queryByText } = setup());
  });

  afterEach(cleanup);

  it('renders component', () => {
    expect(queryByText('{{properCase name}} component made')).toBeInTheDocument();
  });
});
