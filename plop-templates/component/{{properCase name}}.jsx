import React from 'react';
import PropTypes from 'prop-types';

const {{properCase name}} = () => <div>{{ properCase name }} component made</div>;

{{properCase name}}.propTypes = {};

export { {{properCase name}} };
