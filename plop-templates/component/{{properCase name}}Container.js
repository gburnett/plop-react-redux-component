import { connect } from "react-redux";

const mapState = state => ({});
const mapDispatch = {};

const Container = connect(mapState, mapDispatch);

export { Container };
